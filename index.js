function getID(IDname) {
  return document.getElementById(IDname);
}
function getClass(Classname) {
  return document.getElementsByClassName(Classname);
}
//Set value val to all Classname
function classval(Classname, val) {
  for (i = 0; i < Classname.length; i++) {
    document.getElementsByClassName;
    Classname[i].innerText = val;
  }
}

var thuong, ha, dong, haoDong;
var arrChu, arrHo, arrBien;
var tenQuechu, tenQueho, tenQuebien;
var urlChu, urlHo, urlBien;
var vitri;
var dataQue;

//Fetch data quẻ từ json
fetch("./64que.json")
  .then((response) => response.json())
  .then((data) => addDataque(data));

function addDataque(data) {
  dataQue = [...data.que];
  console.table(dataQue);
}
//Chọn số động
var chonDong = true;
function chonSodong() {
  chonDong = getID("isDong").checked;
  if (!chonDong) {
    getID("soDong").classList.add("disabled");
    getID("pills-ho-tab").classList.add("d-none");
    getID("pills-bien-tab").classList.add("d-none");
  } else {
    getID("soDong").classList.remove("disabled");
    getID("pills-ho-tab").classList.remove("d-none");
    getID("pills-bien-tab").classList.remove("d-none");
  }
}
//Gieo ngẫu nhiên: thượng, hạ [1 - 64], hạ [1 - 36]
function gieoNgaunhien() {
  getID("soThuong").value = ranMinmax(1, 64);
  getID("soHa").value = ranMinmax(1, 64);
  if (chonDong) {
    getID("soDong").value = ranMinmax(1, 36);
  }
  gieoQue();
}

function ranMinmax(min, max) {
  let diff = max - min;
  let rand = Math.random();
  rand = Math.floor(rand * diff);
  rand = rand + min;
  return rand;
}

//Tính Quẻ
function gieoQue() {
  var soThuong = getID("soThuong").value * 1;
  var soHa = getID("soHa").value * 1;
  var soDong = getID("soDong").value * 1;

  thuong = soThuong % 8 == 0 ? 8 : soThuong % 8;
  ha = soHa % 8 == 0 ? 8 : soHa % 8;
  dong = soDong % 6 == 0 ? 6 : soDong % 6;

  arrChu = [...batQuai(thuong), ...batQuai(ha)];

  arrHo = [arrChu[5], arrChu[0], arrChu[1], arrChu[4], arrChu[5], arrChu[0]];

  arrBien = [...arrChu];

  var a = doiHao(dong, arrChu);
  arrBien[vitri] = a;

  //Chuyển mã Dec của array binary và lấy tên Quẻ
  tenQuechu = tenQue(chuyenMa(arrChu));
  tenQueho = tenQue(chuyenMa(arrHo));
  tenQuebien = tenQue(chuyenMa(arrBien));
  //set Tab đầu active
  $("#pills-tab li:first-child a").tab("show");
  //vẽ quẻ
  getID("que__chu").innerHTML = veQue(arrChu, dong);
  getID("que__ho").innerHTML = veQue(arrHo, dong);
  getID("que__bien").innerHTML = veQue(arrBien, dong);
  // getID("tenque__chu").innerText = tenQuechu;
  //getID("tenque__ho").innerText = tenQueho;
  // getID("tenque__bien").innerText = tenQuebien;
  urlChu = "/pdf/" + chuyenMa(arrChu) + ".pdf";
  urlHo = "/pdf/" + chuyenMa(arrHo) + ".pdf";
  urlBien = "/pdf/" + chuyenMa(arrBien) + ".pdf";
  PDFObject.embed(urlChu, "#pdf__chu");
  PDFObject.embed(urlChu, "#pdf__ho");
  PDFObject.embed(urlChu, "#pdf__bien");
  classval(getClass("tenque__chu"), tenQuechu);
  classval(getClass("tenque__ho"), tenQueho);
  classval(getClass("tenque__bien"), tenQuebien);
  getID("hao__dong").innerText = "h" + dong;
}
//Lấy tên quẻ có Số quẻ (Dec) là x
function tenQue(x) {
  for (i = 0; i <= dataQue.length; i++) {
    if (dataQue[i].dec == x) {
      return dataQue[i].ten_que;
      break;
    }
  }
}
//Bát quái
function batQuai(x) {
  var quai = [];
  switch (x) {
    case 1:
      quai = [1, 1, 1];
      break;
    case 2:
      quai = [1, 1, 0];
      break;
    case 3:
      quai = [1, 0, 1];
      break;
    case 4:
      quai = [1, 0, 0];
      break;
    case 5:
      quai = [0, 1, 1];
      break;
    case 6:
      quai = [0, 1, 0];
      break;
    case 7:
      quai = [0, 0, 1];
      break;
    case 8:
      quai = [0, 0, 0];
      break;
  }
  return quai;
}
// Đổi hào động x của quẻ
function doiHao(x, que) {
  var n;
  switch (x) {
    case 1:
      n = que[3];
      vitri = 3;
      break;
    case 2:
      n = que[4];
      vitri = 4;
      break;
    case 3:
      n = que[5];
      vitri = 5;
      break;
    case 4:
      n = que[0];
      vitri = 0;
      break;
    case 5:
      n = que[1];
      vitri = 1;
      break;
    case 6:
      n = que[2];
      vitri = 2;
      break;
  }

  return n == 0 ? 1 : 0;
}
//Đổi quẻ qua số Decimal
function chuyenMa(que) {
  var parseArray = (que) => {
    const binaryString = que.join("");
    return parseInt(binaryString, 2);
  };
  return parseArray(que);
}
//Vẽ Quẽ từ h6 ->h1 và highligh hào động vị trí x
function veQue(arrQue, x) {
  console.log(x);
  var queVe = [
    arrQue[3],
    arrQue[4],
    arrQue[5],
    arrQue[0],
    arrQue[1],
    arrQue[2],
  ];
  x = x - 1;
  var queHTML = "";
  for (i = 5; i >= 0; i--) {
    if (queVe[i] == 1) {
      if (i == x) {
        queHTML += '<span class="duong hao__dong"><hr /></span>';
      } else {
        queHTML += '<span class="duong"><hr /></span>';
      }
    } else {
      if (i == x) {
        queHTML += '<span class="am hao__dong"><hr /><hr /></span>';
      } else {
        queHTML += '<span class="am"><hr /><hr /></span>';
      }
    }
    if (i == 3) {
      queHTML += '<span class="line__chia"><hr /></span>';
    }
  }
  return queHTML;
}

/**
 * Bài 7-8
 */

var numArray = [];

var ngauNhien = false;
// Nếu chọn Thêm số ngẫu nhiên thì hiển thị box nhập số lượng số ngẫu nhiên muốn thêm và disable khung nhập số n và ngược lại
function themNgaunhien() {
  ngauNhien = getID("isNgaunhien").checked;
  if (ngauNhien) {
    getID("soluong").classList.remove("d-none");
    getID("soLuongNN").focus();
    getID("soN").classList.add("disabled");
  } else {
    getID("soluong").classList.add("d-none");
    getID("soN").classList.remove("disabled");
    getID("soN").focus();
  }
}

//hàm thêm x số vào mảng M
function addRandom(x, M) {
  for (var i = 1; i <= x; i++) {
    var plusOrminus = Math.random() < 0.5 ? -1 : 1;
    var a = plusOrminus * Math.floor(Math.random() * 1000);
    M.push(a);
  }
}

function themSo() {
  if (ngauNhien) {
    //nếu chọn thêm ngâu nhiên
    var x = getID("soLuongNN").value * 1;
    if (x < 0) {
      alert("Vui lòng nhập số lượng >0");
    } else {
      addRandom(x, numArray);
    }
  } //nếu chọn add từng số
  else {
    var n = getID("soN").value * 1;
    numArray.push(n);
  }
  getID("mangSo").innerText = numArray;
}

function xoaMang() {
  numArray = [];
  getID("mangSo").innerText = numArray;
}

function xoaDau() {
  numArray.shift();
  getID("mangSo").innerText = numArray;
}

function xoaCuoi() {
  numArray.pop();
  getID("mangSo").innerText = numArray;
}
// Bài 01 - Tổng số dương
function tinhTongduong() {
  for (var n = 0, r = 0; r < numArray.length; r++)
    numArray[r] > 0 && (n += numArray[r]);
  getID("tong__duong").innerHTML =
    "Tổng số dương: " + new Intl.NumberFormat().format(n);
}

//Bài 02 - Đếm số dương
function demSoduong() {
  for (var n = 0, r = 0; r < numArray.length; r++) numArray[r] > 0 && n++;
  getID("soluong__duong").innerHTML =
    "Số số dương: " + new Intl.NumberFormat().format(n);
}

//Bài 03 - Tìm số nhỏ nhất
function soMin() {
  for (var n = numArray[0], r = 1; r < numArray.length; r++)
    numArray[r] < n && (n = numArray[r]);
  getID("so__min").innerHTML = "Số nhỏ nhất: " + n;
}

//Bài 04 - Tìm số dương nhỏ nhất
function soduongMin() {
  for (var n = [], r = 0; r < numArray.length; r++)
    numArray[r] > 0 && n.push(numArray[r]);
  if (n.length > 0) {
    for (var e = n[0], r = 1; r < n.length; r++) n[r] < e && (e = n[r]);
    getID("soduong__min").innerHTML = "Số dương nhỏ nhất: " + e;
  } else getID("soduong__min").innerHTML = "Không có số dương trong mảng";
}

//Bài 05 - Tìm số chẵn cuối cùng
function soChancuoi() {
  for (var n = 0, r = 0; r < numArray.length; r++)
    numArray[r] % 2 == 0 && (n = numArray[r]);
  getID("sochan__cuoi").innerHTML = "Số chẵn cuối cùng: " + n;
}
//Bài 06 - Đổi chỗ
// Hàm phụ đổi chỗ 2 vị trí x,y của mảng A
function swap(x, y, arrayA) {
  var e = arrayA[x];
  (arrayA[x] = arrayA[y]), (arrayA[y] = e);
}

function doiCho() {
  swap(getID("viTri1").value, getID("viTri2").value, numArray);
  getID("mang__doicho").innerHTML = "Mảng sau khi đổi: " + numArray;
}

//Bài 07 - Sắp xếp tăng dần
function sortTang() {
  numArray.sort(function (a, b) {
    return a - b;
  });
  getID("mang__tang").innerHTML = "Mảng sau khi sắp xếp: " + numArray;
}

//Bài 08 - Tìm số nguyên tố
function isNguyento(n) {
  if (n < 2) return !1;
  for (var r = 2; r <= Math.sqrt(n); r++) if (n % r == 0) return !1;
  return !0;
}
function soNguyento() {
  for (var n = -1, r = 0; r < numArray.length; r++) {
    if (isNguyento(numArray[r])) {
      n = numArray[r];
      break;
    }
  }
  getID("so__nguyento").innerHTML = "Số nguyên tố đầu tiên: " + n;
}

//Bài 09 - Đếm số nguyên của mảng mới
var numArray2 = [];
var ngauNhien2 = false;

function themNgaunhien2() {
  ngauNhien2 = getID("isNgaunhien2").checked;
  if (ngauNhien2) {
    getID("soluong2").classList.remove("d-none");
    getID("soLuongNN2").focus();
    getID("soN2").classList.add("disabled");
  } else {
    getID("soluong2").classList.add("d-none");
    getID("soN2").classList.remove("disabled");
    getID("soN2").focus();
  }
}

function themSo2() {
  if (ngauNhien2) {
    //nếu chọn thêm ngâu nhiên
    var x = getID("soLuongNN2").value * 1;
    if (x < 0) {
      alert("Vui lòng nhập số lượng >0");
    } else {
      addRandom(x, numArray2);
    }
  } //nếu chọn add từng số
  else {
    var n = getID("soN2").value * 1;
    numArray2.push(n);
  }
  getID("mangSo2").innerText = numArray2;
}

function xoaMang2() {
  numArray2 = [];
  getID("mangSo2").innerText = numArray2;
}

function xoaDau2() {
  numArray2.shift();
  getID("mangSo2").innerText = numArray2;
}

function xoaCuoi2() {
  numArray2.pop();
  getID("mangSo2").innerText = numArray2;
}
function demSonguyen() {
  for (var n = 0, r = 0; r < numArray2.length; r++)
    Number.isInteger(numArray2[r]) && n++;
  getID("so__songuyen").innerHTML = "Số nguyên: " + n;
}

//Bài 10 - So sánh số lượng số âm và số dương
function soAmduong() {
  for (var n = 0, r = 0, e = 0; e < numArray.length; e++)
    numArray[e] > 0 ? n++ : numArray[e] < 0 && r++;
  getID("so__sanh").innerHTML =
    n > r
      ? "Số dương > Số âm"
      : n < r
      ? "Số âm > Số dương"
      : "Số âm = Số dương";
}
